package com.sucy.passive.enchants;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.rit.sucy.service.SuffixGroups;
import com.sucy.passive.ConfigurableEnchantment;
import com.sucy.passive.data.EnchantDefaults;
import com.sucy.passive.data.ItemSets;

public class Life extends ConfigurableEnchantment {

    public Life(Plugin plugin) {
        super(plugin, EnchantDefaults.LIFE, ItemSets.CHESTPLATES.getItems(), 2);
        description = "Grants bonus health when equipped";
        suffixGroups.add(SuffixGroups.HEALTH.getKey());
    }

    @Override
    public void applyEquipEffect(Player player, int level) {
    	player.addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, 999999, level));
    }

    @Override
    public void applyUnequipEffect(Player player, int level) {
        player.removePotionEffect(PotionEffectType.HEALTH_BOOST);
    }
}
