package com.sucy.passive.enchants;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.plugin.Plugin;

import com.rit.sucy.service.SuffixGroups;
import com.sucy.passive.ConfigurableEnchantment;
import com.sucy.passive.data.ConflictGroup;
import com.sucy.passive.data.EnchantDefaults;
import com.sucy.passive.data.ItemSets;

/**
 * Has a chance to knock enemies up into the air on hit
 */
public class Knockup extends ConfigurableEnchantment {

    /**
     * Constructor
     *
     * @param plugin plugin reference
     */
    public Knockup(Plugin plugin) {
        super(plugin, EnchantDefaults.KNOCKUP, ItemSets.SWORDS.getItems(), 10, ConflictGroup.FORCE);
        description = "Has a chance to knock the target into the air";
        suffixGroups.add(SuffixGroups.FORCE.getKey());
    }

    /**
     * Knocks enemies up on hit with a cooldown
     *
     * @param user   player with the enchantment
     * @param target enemy that was hit
     * @param level  enchantment level
     * @param event  event details
     */
    @Override
    public void applyEffect(LivingEntity user, LivingEntity target, int level, EntityDamageByEntityEvent event) {
        if (works(target, user)){
        	
        	// Get a name to use with the cooldown timers
            String name = user instanceof Player ? ((Player)user).getName() : "mob";

            // Make sure the timer isn't null
            if (timers.get(name) == null) timers.put(name, 0l);

            // Check the cooldown
            if (cooldown(level, name, false)) return;

            // Need to knock them up later to get past Minecraft's knockback on taking damage
            new KnockupTask(target, (float)speed(level)).runTaskLater(plugin, 1);
            
            // Update the cooldown timer
            timers.put(name, System.currentTimeMillis());
        }
    }
}