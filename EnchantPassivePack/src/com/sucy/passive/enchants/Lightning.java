package com.sucy.passive.enchants;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.rit.sucy.service.SuffixGroups;
import com.sucy.passive.ConfigurableEnchantment;
import com.sucy.passive.data.EnchantDefaults;
import com.sucy.passive.data.ItemSets;

/**
 * Has a chance to strike lightning on hit
 */
public class Lightning extends ConfigurableEnchantment {

    /**
     * Constructor
     *
     * @param plugin plugin reference
     */
    public Lightning(Plugin plugin) {
        super(plugin, EnchantDefaults.LIGHTNING, ItemSets.AXES.getItems());
        description = "Chance to strike lightning on hit";
        suffixGroups.add(SuffixGroups.LIGHTNING.getKey());
    }

    /**
     * Strikes lightning on hit
     *
     * @param user   player with the enchantment
     * @param target enemy that was hit
     * @param level  enchantment level
     * @param event  event details
     */
    @Override
    public void applyEffect(LivingEntity user, LivingEntity target, int level, EntityDamageByEntityEvent event) {
        if (works(target, user)){
            
        	// Get a name to use with the cooldown timers
            String name = user instanceof Player ? ((Player)user).getName() : "mob";

            // Make sure the timer isn't null
            if (timers.get(name) == null) timers.put(name, 0l);

            // Check the cooldown
            if (cooldown(level, name, false)) return;
        	
            // strike lightning and temporarily protect user
        	user.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20, 100));
        	target.getWorld().strikeLightning(target.getLocation());
            
        	// Update the cooldown timer
            timers.put(name, System.currentTimeMillis());
        }
    }
}
